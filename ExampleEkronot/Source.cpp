#include "Student.h"
#include <iostream>;

using std::cout;
using std::endl;

void dont_copy(Student& s) {

}

int main()
{
	Student* stud1 = new Student(123456789, "Shahar", "Hasson");

	// first student info
	stud1->setGrade(HISTORY_GRADE_IDX, 78);
	stud1->setGrade(MATH_GRADE_IDX, 81);
	stud1->setGrade(LITERATURE_GRADE_IDX, 90);
	stud1->setGrade(ENGLISH_GRADE_IDX, 65);

	Student stud2(*stud1);

	stud2.setGrade(HISTORY_GRADE_IDX, 95);

	const Student* stud3 = new Student(111111);

	//*stud3 = stud2 = *stud1;
	stud1->print();
	stud2.print();
	stud3->print();
	if (*stud1 == stud2) {
		cout << "SAME!!!!!";
	}
	/*
	stud2();
	stud1->print();
	stud2.print();

	Student* stud3 = new Student(111111);
	
	// second student info
	stud2.setGrade(MATH_GRADE_IDX, 87);
	stud2.setGrade(LITERATURE_GRADE_IDX, 90);
	stud2.setGrade(ENGLISH_GRADE_IDX, 98);

	dont_copy(*stud1);


	delete(stud1);*/

	system("pause");
	return 0;

}